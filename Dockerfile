FROM rafa/docker-mesos-master:latest
MAINTAINER Rafael Capucho <rafael.capucho@gmail.com>

RUN apt-get install docker haproxy zip

# mapear alguns -v no docker pra poder reiniciar o nó, exemplo -v /etc/haproxy 
# adicionar o haproxy e baixar o https://github.com/mesosphere/marathon/raw/master/bin/servicerouter.py
# python servicerouter.py --marathon http://173.255.192.122:8080 --syslog-socket /dev/null --haproxy-config /etc/haproxy/haproxy.cfg
# adicionar servicerouter num cronjob
